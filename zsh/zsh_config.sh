# pulled from Chris Titus Tech config + customization

touch "$HOME/.cache/zshhistory"
#-- Setup Alias in $HOME/zsh/aliasrc
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.powerlevel10k
echo 'source ~/.powerlevel10k/powerlevel10k.zsh-theme' >> ~/.zshrc

sudo apt install zsh zsh-syntax-highlighting zsh-autosuggestions

cp .zshrc ~/.zshrc


sudo chsh --shell /bin/zsh $USER

# ----- WHEN DONE RUN ----- #
#autoload -Uz compinstall
#compinstall
